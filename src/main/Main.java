package main;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		System.out.println("Hi, welcome to the trip planner!");
		Scanner sc = new Scanner(System.in);
		System.out.print("What is your name?");
		String name = sc.nextLine();
		System.out.print("Nice to meet you " + name + ", where are you travelling to?");
		String destination = sc.nextLine();
		System.out.println("Great! " + destination + " sounds like a great place!");
		
		System.out.println("How many days are you going to spend travelling");
		int days = sc.nextInt();
		System.out.println("How much money, in USD, are you pallning to spend on your trip?");
		double money = sc.nextDouble();
		
		System.out.println("If you are travelling for " + days + "days that is the same as " + (days *24) +" hours, or " +(days *24*60) + " minutes");
		double averageMoney = money/days;
		System.out.println("If you are going to spend $" + money +" USD that means per day you can spend up to $" + averageMoney +" USD");
		
		System.out.println("What is the time difference, in hours, between your home and " + destination + "?");
		int timeDifference = sc.nextInt();
		System.out.println("That means that when it is midnight at your home, it will be " + timeDifference + ":00 in your tavel destination");
	}
}
